# Frontend for a specific WebSocket Chat

Constants are definded in the file [config.js](./src/config.js) and text to be shown to the user in the file [strings.js](./src/strings.js).

A web server is needed to run this localy. More here: https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/Errors/CORSRequestNotHttp
I use the one in PyCharm.

# Usage

Allows to signup and into the backend provided here: https://chat.ndum.ch/api/docs


# Licence

See LICENCE file