import { Fetcher, ManageLocalStorage, Toast } from "./helpers.js";
import { Strings } from "./strings.js";
import { Config } from "./config.js";

export const UserService = {
    getUsernameInput() {
      return document.getElementById("username").value;
    },
    getPasswordInput() {
      return document.getElementById("password").value;
    },
    isValidUsernameAndPass(username, password) {
        return !(username === "" || password === "");

    },
    async login() {
        const username = this.getUsernameInput();
        const password = this.getPasswordInput();

        if (!this.isValidUsernameAndPass(username, password)) {
            Toast.show(Strings.emptyUsernameOrPass);
            return;
        }
        const url = Config.loginUrl;
        const response = await Fetcher.postData(url, false, {
            "username": username,
            "password": password
        });
        if (response.error) {
            console.error(response.error);
            if (response.error.status === 401) {
                Toast.show(Strings.userNotFound);
                return;
            } else if (!response.error.ok) {
                Toast.show(Strings.serverResponse);
                return;
            }
            return;
        }
        ManageLocalStorage.setToken(response.token);
        ManageLocalStorage.setUserId(response.userId);
        ManageLocalStorage.setUsername(username);
        window.location.href = "static/chat.html";
    },
    async register() {
        const username = this.getUsernameInput();
        const password = this.getPasswordInput();

        if (!this.isValidUsernameAndPass(username, password)) {
            Toast.show(Strings.emptyUsernameOrPass);
            return;
        }
        const url = Config.registerUrl;
        const response = await Fetcher.postData(url, false,{
            "username": username,
            "password": password
        });
        if (response.error) {
            console.error(response.error);
            if (response.error.status === 400) {
                Toast.show(Strings.usernameAlreadyTaken);
                return;
            } else if (!response.error.ok) {
                Toast.show(Strings.serverResponse);
                return;
            }
            return;
        }
        Toast.show(Strings.successfulSignup);
    },
    logout() {
        ManageLocalStorage.removeEverything();
        window.location.href = "../index.html";
    }
}