import { UserService } from "./userService.js";

export const Fetcher = {
    async getData(url) {
        try {
            const response = await fetch(url, { method: "GET", });
            if (!response.ok) {
                console.error("status", response.status);
                return { error: response }
            }
            const data = await response.json();
            if (data.error) {
                console.error(data.error);
                return { error: data.error }
            }
            return data;

        } catch (error) {
            return { error: error }
        }
    },
    async postData(url, needs_auth, data) {
        const headers = new Headers();
        if (needs_auth) {
            const token = ManageLocalStorage.getToken();
            headers.append('Authorization', `Bearer ${token}`);
        }
        headers.append('Content-Type', 'application/json');
        let init = {
            method: "POST",
            headers: headers,
            body: JSON.stringify(data)
        };
        try {
            const response = await fetch(url, init);
            if (!response.ok) {
                console.error("status", response.status);
                return { error: response }
            }
            const data = await response.json();
            if (data.error) {
                console.error(data.error);
                return { error: data.error }
            }
            return data;

        } catch (error) {
            return { error: error }
        }
    }
}

export const Toast = {
    show(msg) {
        let toast = document.getElementById("toast");
        const message = document.createElement('p');
        message.textContent = msg;
        toast.appendChild(message)
        toast.style.visibility = "visible";
        setTimeout(function(){
            toast.style.visibility = "hidden";
            toast.innerText = "";
        }, 5000);
    }
}

export const CurrentTime = {
    timeString() {
        const now = new Date();
        const hours = now.getHours();
        const minutes = now.getMinutes();
        const seconds = now.getSeconds();
        return `${hours}:${minutes}:${seconds}`
    },
    utcNowString() {
        return new Date().toISOString();
    }
}

export const ManageLocalStorage = {
    getValueByKey(key) {
        let value = localStorage.getItem(key);
        if (value == null) {
            return '';
        }
        return value;
    },
    setValueByKey(key, value) {
        localStorage.setItem(key, value);
    },
    handleKeyNotFound(key) {
        console.error(`Key: '${key}' not found in localStorage, sending user to login`);
        UserService.logout();
    },
    getToken() {
        const key = 'token';
        const token = this.getValueByKey(key);
        if (token !== ''){
            return token;
        }
        this.handleKeyNotFound(key)

    },
    setToken(token) {
        this.setValueByKey('token', token);
    },
    getUserId() {
        const key = 'userId';
        const userId = this.getValueByKey(key);
        if (userId !== ''){
            return userId
        }
        this.handleKeyNotFound(key)
    },
    setUserId(userId) {
        this.setValueByKey('userId', userId);
    },
    getUsername() {
        const key = 'username';
        const username = this.getValueByKey(key);
        if (username !== ''){
            return username
        }
        this.handleKeyNotFound(key)
    },
    setUsername(username) {
        this.setValueByKey('username', username);
    },
    getUsersList() {
        const users = this.getValueByKey('users');
        if (users !== ''){
            return JSON.parse(users);
        }
    },
    setUsersList(users) {
        this.setValueByKey('users', JSON.stringify(users));
    },
    removeEverything() {
        for (const item of ['token', 'userId', 'username', 'users']) {
            if (localStorage.getItem(item) !== null) {
                localStorage.removeItem(item);
            }
        }
    }
}