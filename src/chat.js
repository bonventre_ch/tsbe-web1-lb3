import { Config } from "./config.js";
import { ManageLocalStorage } from "./helpers.js";
import { UserService } from "./userService.js";
import { SocketService } from "./socketService.js";
import { ChatService } from "./chatService.js";

document.addEventListener("DOMContentLoaded", function () {

    // check if current user is logged in
    ManageLocalStorage.getToken();
    ManageLocalStorage.getUserId();

    // start the websocket
    const webSocket = new WebSocket(Config.websocketUrl);
    webSocket.onopen = () => SocketService.onOpen();
    webSocket.onclose = (event) => SocketService.onClose(event);
    webSocket.onerror = (event) => SocketService.onError(event);
    webSocket.onmessage = (event) => SocketService.onMsg(event);

    // check if the buttons have been pressed
    const send_button = document.getElementById('send_msg');
    send_button.addEventListener('click', async function () {
        await ChatService.validateAndSendMsg()
    });

    const logout_button = document.getElementById('logout');
    logout_button.addEventListener('click', async function () {
        UserService.logout();
    });
});