import { UserService } from "./userService.js";

// document ready
document.addEventListener("DOMContentLoaded", function() {

    const login_button = document.getElementById('login');
    login_button.addEventListener('click', async function () {
        await UserService.login();
    });

    const register_button = document.getElementById('register');
    register_button.addEventListener('click', async function () {
        await UserService.register();
    });
});