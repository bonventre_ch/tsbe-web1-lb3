const baseUrl = 'https://chat.ndum.ch/api/';

export const Config = {
   baseUrl: baseUrl,
   messagesUrl: baseUrl + 'messages',
   usersUrl: baseUrl + 'users',
   authUrl: baseUrl + 'auth',
   loginUrl: baseUrl + 'auth/login',
   registerUrl: baseUrl + 'auth/register',
   websocketUrl: 'wss://chat.ndum.ch'
};
