import { Strings } from "./strings.js";
import { CurrentTime, ManageLocalStorage, Toast } from "./helpers.js";
import { Message } from "./types.js";
import { ChatService } from "./chatService.js";

export const SocketService = {
    async onOpen() {
        const backend_users = await ChatService.getChatUsers();
        ManageLocalStorage.setUsersList(backend_users);
        ChatService.updateUsernames();
        const previous_messages = await ChatService.getOldMessages();
        let messages = []
        for (const entry of previous_messages) {
            messages.push(entry);
        }
        const chat_history = document.getElementById("chat");
        chat_history.replaceChildren();
        for (const message of messages) {
            const msg = new Message(
                message._id,
                message.username,
                message.message,
                message.createdAt
            );
            ChatService.createChatMessage(false, msg);
        }
        const msg = new Message(
            1,
            "",
            `Chat gestartet um: ${CurrentTime.timeString()}`,
            CurrentTime.utcNowString()
        );
        ChatService.createChatMessage(true, msg);
        Toast.show(Strings.welcome);
    },

    onClose(event) {
        console.error('error: onClose');
        console.error(event);
        window.location.href = "chat.html";
    },

    onError(event) {
        console.error(`error: socket.onerror`);
        console.error(event);
        window.location.href = "chat.html";
    },

    async onMsg(event) {
        const new_msg = JSON.parse(event.data).data;
        // check if new user joined the chat
        if (new_msg.username && !new_msg.message) {
            await ChatService.updateUsers();
            return;
        }
        const msg = new Message(new_msg._id, new_msg.username, new_msg.message, new_msg.createdAt);
        ChatService.createChatMessage(false, msg);
        ChatService.updateUsernames();
    }
}