// misusing these as types
export class Message {
    constructor(id, username, text, date_utc) {
        this.id = id
        this.username = username
        this.text = text
        this.date_utc = date_utc
    }
}

export class User {
    constructor(id, username, createdAt) {
        this.id = id
        this.username = username
        this.createdAt = createdAt
    }
}