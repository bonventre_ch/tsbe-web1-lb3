export const Strings = {
    welcome: 'Willkommen im Klassen Chat',
    emptyMsg: 'Schreibe zuerst eine Nachricht!',
    emptyUsernameOrPass: "Befülle 'Benutzername' und 'Passwort'!",
    userNotFound: 'Benutzer mit diesen Anmeldedaten existiert nicht!',
    usernameAlreadyTaken: 'Benutzer mit diesem Namen existiert schon! Melde dich an oder wähle einen anderen Namen!',
    successfulSignup: 'Registrierung war erfolgreich. Du kannst dich jetzt anmelden!',
    serverResponse: 'Der Server kann die Anfrage nicht bearbeiten!'
};