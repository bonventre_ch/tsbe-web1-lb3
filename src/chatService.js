import { CurrentTime, Fetcher, ManageLocalStorage, Toast } from "./helpers.js";
import { Strings } from "./strings.js";
import { Config } from "./config.js";
import { Message } from "./types.js";

export const ChatService = {
    async validateAndSendMsg() {
        const message = document.getElementById("message");
        if (message.value === "") {
            Toast.show(Strings.emptyMsg);
            return;
        }
        const url = Config.messagesUrl;
        const response = await Fetcher.postData(url, true, { message: message.value });
        // empty the input field
        message.value = "";
    },
    async getOldMessages() {
        try {
            const url = Config.messagesUrl;
            return await Fetcher.getData(url);
        } catch (error) {
            console.error(error)
        }
    },
    async getChatUsers() {
       try {
           const url = Config.usersUrl;
           return await Fetcher.getData(url);
       } catch (error) {
           console.error(error)
       }
    },
    createChatMessage(is_system, msg) {
        const section = document.createElement('section');
        const username = ManageLocalStorage.getUsername();
        if (username === msg.username) {
            section.classList.add("message_self");
        } else if (is_system) {
            section.classList.add("message_system");
        } else {
            section.classList.add("message_user");
        }

        if (!is_system) {
            const message_username = document.createElement('p');
            message_username.classList.add("message_username");
            message_username.textContent = msg.username;
            section.appendChild(message_username);
        }

        const message_content = document.createElement('p');
        message_content.classList.add("message_content");
        message_content.textContent = msg.text;
        section.appendChild(message_content);

        const message_time = document.createElement('p');
        message_time.classList.add("message_time");
        const date = new Date(msg.date_utc);
        message_time.textContent = date.toLocaleString('de-CH', { timeZone: 'Europe/Zurich' });
        section.appendChild(message_time);

        const message = document.createElement('li');
        message.appendChild(section);
        const chat = document.getElementById("chat");
        chat.appendChild(message);
    },
    async updateUsers() {
        const local_users = ManageLocalStorage.getUsersList();
        const backend_users = await this.getChatUsers();
        for (const user of backend_users) {
            const key = "username";
            const value = user.username;
            const index = local_users.findIndex(obj => obj[key] === value);
            // -1 means not found, so add them
            if (index === -1) {
                local_users.push(user);
                const msg = new Message(
                    1,
                    "",
                    `Neuer Benutzer mit dem Namen '${user.username}' ist beigetreten!`,
                    CurrentTime.utcNowString()
                );

                this.createChatMessage(true, msg);
            }
        }
        ManageLocalStorage.setUsersList(local_users);
    },
    updateUsernames() {
        const users_list = document.getElementById('usernames');
        users_list.replaceChildren();
        const current_timestamp_in_sec = Math.floor(Date.now() / 1000);
        const sixty_minutes_ago = current_timestamp_in_sec - 3600;
        const local_users = ManageLocalStorage.getUsersList();
        for (const user of local_users) {
            const last_online_in_sec = Math.floor(Date.parse(user.updated_at) / 1000);
            const entry = document.createElement('li');
            const username = document.createElement('p');
            username.setAttribute("id", user.username);
            username.classList.add("user");
            username.textContent = user.username;
            if (sixty_minutes_ago > last_online_in_sec) {
                username.classList.add("offline");
            } else {
                username.classList.add("online");
            }
            entry.appendChild(username);
            users_list.appendChild(entry);
        }
    }
}